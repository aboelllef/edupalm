function auto_complete(field,table,data,label ,controller ,rtl){
	fill_table(field, table, data.sort(), label, controller, rtl);
	field.addEventListener('change', function(e) {
		var pattern = e.source.value;
		var results= search(data, pattern);
		fill_table(field, table, results, label, controller, rtl);
	});
};

//Returns the array which contains a match with the pattern
function search(data, pattern){
	var search_results = [];
	var search_text = pattern.toLowerCase(); 
	for (var i = 0; i < data.length; i++) {
		var row_title_lower_cased = data[i].name.toLowerCase();
		if(row_title_lower_cased.search(search_text)>-1){
			search_results.push(data[i]);
		};
		// if (data[i].substring(0, pattern.length).toUpperCase() === pattern.toUpperCase()) {
			// search_results.push(data[i]);
		// }
	}
	return search_results.sort();
}

//setting the tableview values
function fill_table(field, table, results, label, controller, rtl) {
	if (results.length) {
		if (controller == "courses") {label.text = L("courses_view_help_text");};
		if (controller == "charts") {label.text = L("charts_view_help_text");};
	} else{
		label.text = L("no_search_results");
	};
	var table_data = [];
	for (var i = 0; i < results.length; i++) {

		var row_label = Ti.UI.createLabel({
			text : results[i].name,
			color: 'white',			
			left : '5%'
		});
		var row = Ti.UI.createTableViewRow({
			backgroundImage:'/images/table_rows/table_row_bg.png',
			height : '40',
			course_id:results[i].id
		});
		if(rtl){
			row_label.right="5%";
			row_label.textAlign="right";
			row.backgroundImage="/images/table_rows/table_row_bg_rtl.png";
		};
		row.add(row_label);
		table_data.push(row);
	};
	table.setData(table_data);
};
	
exports.auto_complete = auto_complete;