exports.get = function (link, params,callback, error_callback){
	Ti.App.fireEvent("show_loading");
	url	=  Alloy.Globals.domain_name+ link	;
	xhr = Ti.Network.createHTTPClient({
		onload:function(e) {
			var response_data = JSON.parse(this.responseText);
			Ti.App.fireEvent("hide_loading");
			callback(response_data);
   		},
	    onerror: function(e) {
	    	handle_errors(e,error_callback);
	    	// var errorDialouge = Ti.UI.createAlertDialog({
	            // title : L('alert'),
	            // message : L("connection_error"),
	            // cancel : 0,
	            // buttonNames : [L("ok_button"), L("try_button")]
	        // });
	        // errorDialouge.addEventListener('click', function(e) {
	            // if (e.index != 0) {
	            	// xhr.open("GET",url);
	            	// xhr.setRequestHeader('Content-Type','application/json');
	            	// xhr.setRequestHeader('Authorization','Bearer '+ Alloy.Globals.access_token);
	            	// xhr.send(params);	
	            // }
	        // });
	        // errorDialouge.show();
	    },
	    timeout: 10000
	});
	xhr.open("GET",url);
	xhr.setRequestHeader('Content-Type','application/json');
	xhr.setRequestHeader('Authorization','Bearer '+Alloy.Globals.access_token);
	xhr.send(params);	
};
exports.post = function (link,params,callback,error_callback){
	Ti.App.fireEvent("show_loading");
	url	=  Alloy.Globals.domain_name+ link	;
	xhr = Ti.Network.createHTTPClient({
		onload:function(e) {
			var response_data = JSON.parse(this.responseText);
			Ti.App.fireEvent("hide_loading");
			callback(response_data);
			
   		},
	    onerror: function(e) {
	    	handle_errors(e,error_callback);
	    	// var errorDialouge = Ti.UI.createAlertDialog({
	            // title : L('alert'),
	            // message : L("connection_error"),
	            // cancel : 0,
	            // buttonNames : [L("ok_button"), L("try_button")]
	        // });
	        // errorDialouge.addEventListener('click', function(e) {
	            // if (e.index != 0) {
	            	// xhr.open("POST",url);
	            	// if(authorized){
	            		// xhr.setRequestHeader('Content-Type','application/json');
	            		// xhr.setRequestHeader('Authorization','Bearer '+ Alloy.Globals.access_token);
	            	// }
	            	// xhr.send(params);	
	            // };
	        // });
	        // errorDialouge.show();
	    },
	    timeout: 15000
	});
	xhr.open("POST",url);
	xhr.setRequestHeader('Authorization','Bearer '+ Alloy.Globals.access_token);
	xhr.send(params);	
};

function handle_errors(e,error_callback){
	Ti.App.fireEvent("hide_loading");
	if (error_callback) {
		error_callback(e);
	} else{
		Alloy.Globals.handle_network_failure(e);
	};
	
}
