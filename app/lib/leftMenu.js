tasks = require('commenTasks');
var currentView;
var views_container ;
var params ;
var main_window;
function init(container,top_bar,left_button,window){
	views_container = container;
	main_window = window;
	params = { "views_container":views_container,"top_bar":top_bar,"left_button":left_button};
};

function formMenuData(){

leftMenuRows = [];
menuData = [
		{title: 'left_menu_tests',		customView: 'courses',		image: '/images/menu_icons/courses_icon.png'},
		{title: 'left_menu_atttendance',		customView: 'attendance',	image: "/images/menu_icons/attendance_icon.png"},
		{title: 'left_menu_tracking_charts',	customView: 'charts',		image:'/images/menu_icons/charts_icon.png'},
		{title: 'left_menu_settings',			customView: 'settings',		image:'/images/menu_icons/settings_icon.png'},
		{title: 'left_menu_logout',			customView: null,		image:'/images/menu_icons/logout_icon.png'}
		
];
	for (var i=0; i < menuData.length; i++) {
	  row = Ti.UI.createTableViewRow({
	  	layout: 'horizontal',
	  	height:50,
	  	customView  : menuData[i].customView,
	  	leftImage: menuData[i].image
	  });
	  var rowText = Ti.UI.createLabel({
	  	text:L(menuData[i].title),
	  	top:'25%',
	  	left:100,
	  	color:'white'
	  });

	  row.add(rowText);
	  leftMenuRows.push(row);
  	};
	return leftMenuRows;	
};

function openView(controllerName,extras){
	
	if (currentView){
		views_container.remove(currentView);
	};
	for (var attrname in extras) { params[attrname] = extras[attrname]; }
	currentView = Alloy.createController(controllerName,params).getView();
	views_container.add(currentView);
	
	if(controllerName != 'settings' ){
		tasks.check_connection(main_window);
	}
};

function rowSelect(e) {
	if (currentView.id != e.row.customView) {
		if (e.rowData.customView == null){
			Ti.App.Properties.setString("access_token",null);
			openView('login');
		}else{
			openView(e.rowData.customView);
		}
	}
};

exports.current_view = currentView;
exports.init=init;
exports.formMenuData = formMenuData;
exports.openView = openView;
exports.rowSelect = rowSelect;