// var host = "162.243.146.127:9000" ;
var host = "192.168.1.7:8000" ;
var url = "http://"+host; 
var xhr ;
var response ;


(function(){
	
	xhr = Ti.Network.createHTTPClient({
		onload:function(e) {
	        Ti.API.info(this.responseText);
	        response = this.responseText;
   		},
	    onerror: function(e) {
	    	alert(e.error);
	    },
	    timeout: 10000
	});
	// xhr.setRequestHeader('Content-Type','application/json');

}());
function get(route,params){
	xhr.open("GET",url+route);
	xhr.send(params);
};
function post(route,params){
	xhr.open("POST",url+route);
	xhr.send(params);
};
function put(route,params){
	xhr.open("PUT",url+route);
	xhr.send(params);
};
function del(route,params){
	xhr.open("DELETE",url+route);
	xhr.send(params);
};


exports.get = get ;
exports.response = response;
exports.post = post ;
exports.put = put ;
exports.del = del ;

