function capitalizeWord(word) {
	return word.charAt(0).toUpperCase() + word.slice(1).toLowerCase();
};

function capitalizeText(text) {
	var capitalizedText = "";
	wordsArray = text.split(" ");
	for (var i = 0; i < wordsArray.length; i++) {
		word = capitalizeWord(wordsArray[i]);
		capitalizedText = capitalizedText + " " + word;
	};
	return capitalizedText;
};

function update_top_bar(bar, left_button, label, right_button) {
	var bar_children = bar.children;
	for (var i = 1; i < bar_children.length; i++) {
		bar.remove(bar_children[i]);
	};
	bar.add(left_button);
	bar.add(label);
	bar.add(right_button);
	return bar;
};

function check_connection(main_window) {
	if (Titanium.Network.networkType != Titanium.Network.NETWORK_NONE) {
		// TODO send answer if existed answers
		var database = require('database');
	} else {
		show_exit_dialog(main_window);
	}
};

function show_exit_dialog(main_window) {
	var modal_win = Ti.UI.createWindow({
		modal : true,
		backgroundColor : "#000",
		opacity : "0.7",
		navBarHidden : true
	});

	var container_view = Ti.UI.createView({
		height : "170",
		width : "80%",
		backgroundImage : '/images/box_text.png',
	});
	
	var message_lable = Ti.UI.createLabel({
		text : L("alert_connection_error"),
		color : "white",
		top : "5%",
		width : "auto",
		height : 'auto',
		textAlign : 'center',
		font : {
			fontSize : "18sp"
		}
	});
	
	var exit_button = Ti.UI.createButton({
		title : 'Ok',
		bottom : "7%",
		width : "100",
		height : "40",
		color : '#ffffff',
		backgroundColor : "#FF9100"
	});
	
	container_view.add(message_lable);
	container_view.add(exit_button);
	modal_win.add(container_view);
	modal_win.open();
	
	exit_button.addEventListener('click', function(e) {
		modal_win.close();
		exit_app(main_window);
	});
	
	modal_win.addEventListener('android:back', function(e) {
		modal_win.close();
		exit_app(main_window);
	});
};

function exit_app(main_window) {
	main_window.close();
}

exports.check_connection = check_connection;
exports.capitalizeText = capitalizeText;
exports.capitalizeWord = capitalizeWord;
exports.update_bar = update_top_bar;