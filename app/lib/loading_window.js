var indWin = Ti.UI.createWindow({
	backgroundColor : '#111111',
	opacity : 0.7,
	fullscreen : false,
	navBarHidden : true
});

var indView = Ti.UI.createView({
	layout : 'vertical',
	backgroundColor : '#000',
	borderRadius : 10,
	opacity : 0.9,
	height : 80,
	width : 100
});

var activityIndicator = Ti.UI.createActivityIndicator({
	style : Ti.UI.iPhone.ActivityIndicatorStyle.BIG,
	top : 10,
	height : Ti.UI.SIZE,
	width : Ti.UI.SIZE
});


indView.add(activityIndicator);
indWin.add(indView);

function hide() {
	setTimeout(function() {
		indWin.close();
		activityIndicator.hide();
	},200);

};

function show() {
	for (var i=1; i < indWin.children.length; i++) {
	  indWin.remove(indWin.children[i]);
	};
	indWin.open();
	activityIndicator.show();

};


exports.show = show;
exports.hide = hide;