  exports.pagingControl = function(scrollableView,top_bar,questions_length,can_submit) {
  	var submit_button = Ti.UI.createButton({
		backgroundColor: "transparent",
		backgroundImage: "/images/submit_icon.png",
		right: "0",
		height:'38',
		width: "40",	
		visible:false
	});
	if (can_submit) {
		submit_button.addEventListener("click",function(e){
			submit_button.hide();
			viewRightArrow.hide();
	        viewLeftArrow.hide();			
			Ti.App.fireEvent("app:submit_test");
		});
		
	} else{
		submit_button.addEventListener("click",function(e){
			submit_button.hide();
			viewRightArrow.hide();
	        viewLeftArrow.hide();
			Ti.App.fireEvent("app:open_home");
		});
		
	};
	top_bar.add(submit_button);
	
  	var currentPage = 0;
    var viewLeftArrow = Ti.UI.createView({
    	id:		"leftArrow",
        height: "38",
        width: 	"40",
        left: 	"0",
        top:'1',
        zIndex:	"10"
    });
 
    var imageLeftArrow = Ti.UI.createImageView({
        image  :'/images/left_arrow.png',
        height :Ti.UI.SIZE,
        width  :Ti.UI.SIZE
    });
    viewLeftArrow.add(imageLeftArrow);
  
    var viewRightArrow = Ti.UI.createView({
    	id		:"rightArrow",
        height 	:"38",
        width  	:"40",
        right  	:"0",
        top:"1",
        zIndex 	: 10
    });
 
    var imageRightArrow = Ti.UI.createImageView({
        image : '/images/right_arrow.png',
        height : Ti.UI.SIZE,
        width : Ti.UI.SIZE,
    });

    viewRightArrow.add(imageRightArrow);

    var numberOfPages = scrollableView.getViews().length;
 
    if (numberOfPages == 1) {
        viewRightArrow.visible = false;
		viewLeftArrow.visible = false;
    } else {
        viewRightArrow.visible = true;
		viewLeftArrow.visible = false;
    };
 
    // Callbacks
    function right_icon_handler(currentPageNumber){
		if (currentPageNumber == numberOfPages-1 ) {
            viewRightArrow.visible = false;
            viewLeftArrow.visible = true ;
            submit_button.visible = true ;
		}else{
            viewRightArrow.visible = true;
            viewLeftArrow.visible = true;
		};
    	top_bar.children[1].text= L("question") + (scrollableView.getViews()[currentPageNumber].question_order)+ L("of") + questions_length;

    };
    function left_icon_handler(currentPageNumber){
		if (currentPageNumber == 0 ) {
	        viewRightArrow.visible = true;
	        viewLeftArrow.visible = false;
           // add submit button
    	}else{
            viewRightArrow.visible = true;
            viewLeftArrow.visible = true;
            submit_button.visible = false ;    	        			
    	};
    	top_bar.children[1].text= L("question") + (scrollableView.getViews()[currentPageNumber].question_order)+ L("of") + questions_length;
    };
    rightClick = function(event) {
					currentPageNumber = scrollableView.currentPage;
			        scrollableView.scrollToView(currentPageNumber+1);
       			};
    leftClick = function(event) {
    		currentPageNumber = scrollableView.currentPage;       		
            scrollableView.scrollToView(currentPageNumber-1);
	        	};
            
    viewRightArrow.addEventListener("click", rightClick);
    viewLeftArrow.addEventListener("click", leftClick);
    scrollableView.addEventListener("scroll", function (e){
    	if(e.currentPage > currentPage){
			right_icon_handler(e.currentPage);	
    	}else{
    		if(e.currentPage < currentPage){
    			left_icon_handler(e.currentPage);
    		};
    	} ; 
    	currentPage = e.currentPage;
    });
    
    
 
    return [viewLeftArrow,viewRightArrow];
};