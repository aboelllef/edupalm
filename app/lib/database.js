var db = Ti.Database.open('edu_palm_db');
db.execute('CREATE TABLE IF NOT EXISTS manuscripts_table( manuscript_id INTEGER PRIMARY KEY, question_id INTEGER , correct_answer INTEGER, student_answer INTEGER);');
db.close();

exports.prepare_answers_to_submit = prepare_answers_to_submit;

exports.update = update ;
exports.clear = clear ;
exports.list = list ;
exports.add = add ;
exports.del = del ;


		
function prepare_answers_to_submit(){
	var list = {};
	var db = Ti.Database.open('edu_palm_db');
	var result = db.execute('SELECT * FROM manuscripts_table');
	while (result.isValidRow()){
		var key = "question-"+result.fieldByName('question_id')+
			"-manuscript-"+ result.fieldByName('manuscript_id')+
			"-answer";
		list[key] = result.fieldByName('student_answer');
		result.next();
	}
	result.close();
	db.close();

	return list;
};

function list () {
	var list = [];
	var db = Ti.Database.open('edu_palm_db');
	var result = db.execute('SELECT * FROM manuscripts_table');
	while (result.isValidRow()) {
		var row = {
			question_id : result.fieldByName('question_id'),
			manuscript_id : result.fieldByName('manuscript_id'),
			correct_answer : result.fieldByName('correct_answer'),
			student_answer : result.fieldByName('student_answer')
		};
		Ti.API.info( "question_id : "+row['question_id']+" manuscript_id : "+row["manuscript_id"]+
			" correct_answer : "+row["correct_answer"]+
			" student_answer : "+row["student_answer"]);
		list.push(row);
		result.next();
	}
	result.close();
	db.close();

	return list;
};

function add (manuscript_id, question_id, correct_answer, student_answer) {
		var db = Ti.Database.open('edu_palm_db');
		db.execute("INSERT INTO manuscripts_table(manuscript_id, question_id, correct_answer, student_answer) VALUES(?,?,?,?)",
						manuscript_id, question_id, correct_answer, student_answer);
		db.close();
};

function del(question_id) {
	try {
		var db = Ti.Database.open('edu_palm_db');
		db.execute("DELETE FROM manuscripts_table WHERE manuscript_id = ?",
				question_id);
		db.close();
	} catch (e) {
		alert(L("alert_database_delete_error"));
	}
};

function clear () {
	try {
		var db = Ti.Database.open('edu_palm_db');
		db.execute("DELETE FROM manuscripts_table");
		db.close();
	} catch (e) {
		alert(L("alert_database_clear_error"));
	}
};
function update (student_answer, manuscript_id) {
	try{
		var db = Ti.Database.open('edu_palm_db');
		db.execute("UPDATE manuscripts_table SET student_answer = ? WHERE manuscript_id = ?",
						student_answer, manuscript_id);
		row = db.execute("Select * FROM manuscripts_table WHERE manuscript_id = ?",manuscript_id);
		Ti.API.info("id : "+row.fieldByName("manuscript_id")+", question_id : "+ row.fieldByName("question_id")+", student_answer : "+row.fieldByName("student_answer"));
		db.close();
		return true;
	 }catch (e) {
		 alert(L("alert_database_update_error"));
		 return false;
	 }
};