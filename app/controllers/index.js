Ti.UI.setBackgroundColor('transparent');
var requester = require("requests_handler");
var loading = require("loading_window");

var tasks = require('commenTasks');

former = require('leftMenu');
//Alloy.Globals.domain_name = "http://162.243.146.127:9000/";
Alloy.Globals.domain_name = "http://192.168.1.104:8000/";


Alloy.Globals.is_rtl = is_rtl;
Alloy.Globals.handle_network_failure = handle_network_failure; 
Alloy.Globals.win = $.win;
function is_rtl() {
	custom_locale = Ti.App.Properties.getString("custom_locale");
	var rtl = false;
	if ("ar" == custom_locale) {
		rtl = true;
	}
	return rtl;
};

function handle_network_failure(e,message){
	switch(e.source.status){
	case 403:
		alert("you are not allowed to talk this action");
	  break;
	case 500:
	  	alert(e.error);
	  break;
	case 400:
		response = message || "Error happened while submitting data, Please, Try Again";
	  	alert(response);
	  break;
	default:
	//alert("Network Error, Please check you internet connection and try Again");
	alert(JSON.stringify(e));
	  // TODO dismiss spinner	  
	}
	
};

force_locale();
former.init($.ds.contentview, $.ds.navview, $.ds.leftButton, $.win);
authenticate();
var leftMenuTable;
add_leftmenu_data();

leftMenuTable.addEventListener('click', function selectRow(e) {
	former.rowSelect(e);
	$.ds.toggleLeftSlider();
});

Ti.App.addEventListener("app:openForgetPasswordForm", function(e) {	former.openView('forgetPassword');});

Ti.App.addEventListener("app:openSignupForm", function(e) {	former.openView('signup');});

Ti.App.addEventListener("app:open_home", function(e) {	former.openView('courses');});

Ti.App.addEventListener("app:openLoginForm", function(e) {	former.openView('login');});

Ti.App.addEventListener("app:openQuiz", function(e) {	former.openView('quizzes');});

Ti.App.addEventListener("app:open_tests", function(data) {	former.openView('tests', data);});

Ti.App.addEventListener("app:open_test_info", function(data) {	former.openView('tests/quiz_info', data);});

Ti.App.addEventListener("app:startQuiz", function(data) { former.openView('tests/questions', data); });

Ti.App.addEventListener("app:submit_test", function(e) {	is_questions = false; });

Ti.App.addEventListener("app:quizTimeOut", function(d){
	var win = Alloy.createController("shared/modal").getView();
	win.open();
});

Ti.App.addEventListener("show_loading",function(e){
	loading.show();
});
Ti.App.addEventListener("hide_loading",function(e){
	loading.hide();
});

Ti.App.addEventListener("app:open_settings", function(data){
	former.openView('settings', data);
	leftMenuTable.setData(former.formMenuData());
});

$.win.orientationModes = [ Titanium.UI.PORTRAIT ];
$.win.open({
	transition : Titanium.UI.iPhone.AnimationStyle.FLIP_FROM_LEFT
});

tasks.check_connection($.win);

function authenticate() {
	var access_token = Ti.App.Properties.getString("access_token");
	if (access_token) {
		Alloy.Globals.access_token = access_token;
		fetch_student_info();
	} else {
		former.openView('login');
	}
};
function force_locale() {
	locale = require("com.shareourideas.locale");
	custom_locale = Ti.App.Properties.getString("custom_locale") || "en";
	locale.setLocale(custom_locale);
};

function add_leftmenu_data(){
	leftMenuTable = Ti.UI.createTableView({
		data : former.formMenuData(),
		backgroundColor: 'transparent',
		separatorColor: 'transparent'
	});
	$.ds.leftMenu.add(leftMenuTable);
};
function fetch_student_info(){
	var callback = function(info){
		former.openView("courses");
		$.ds.student_name_menu.text = info.username;
		$.ds.student_desc_menu.text = info.group_name+" - "+info.level_name;
		$.ds.userPic.setImage(Alloy.Globals.domain_name+"media/"+info.image);	
	};
	var error_callback = function(){
		former.openView('login');
	};
	requester.get("api/students/" + Ti.App.Properties.getString("student_pk") + "/",{},callback,error_callback);
};
exports.fetch_student_info = fetch_student_info;