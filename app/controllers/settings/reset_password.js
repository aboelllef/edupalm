var rtl = Alloy.Globals.is_rtl();
if(rtl) { handle_rtl_changes(); };

function handle_rtl_changes(){
	$.old_password_label.textAlign = "right";
	$.new_password_label.textAlign = "right";
	$.confirm_password_label.textAlign = "right";
	$.old_password_field.textAlign = "right";
	$.new_password_field.textAlign = "right";
	$.confirm_password_field.textAlign = "right";
	$.mandatory_label.textAlign = 'right';	
};
function reset_password(){
	var passwords = collect_passwords();
	if (passwords[0].replace(/\s/g, "")){
		if(passwords[1]==passwords[2]){
			var requester = require("requests_handler");
			var callback=function(){
				alert(L("alert_password_changed"));
				Ti.App.fireEvent("app:open_settings");
				
			};
			var error_callback = function(e){
				var message = L("wrong_old_password");
				Alloy.Globals.handle_network_failure(e,message);
			};
			var params = {old_password:passwords[0], new_password1: passwords[1], new_password2: passwords[2]};
			var url = "api/students/"+Ti.App.Properties.getString("student_pk")+"/set_password/";
			requester.post(url,params,callback,error_callback);			
		}else{
			alert(L("un_matched_passwords"));
		};
	}else{
		alert(L("enter_old_passowrd"));
	};
};

function collect_passwords(){
	var passwords = [];
	passwords.push($.old_password_field.value);
	passwords.push($.new_password_field.value);
	passwords.push($.confirm_password_field.value);

	return passwords;
};
function close_window(){
	$.reset_password_window.close();
};
