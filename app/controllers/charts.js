var auto_complete = require('autocomplete');
var args = arguments[0]||{};

var top_bar= args["top_bar"];
var container = args["views_container"];
var left_button = args.left_button;
var requester = require("requests_handler");

var rtl = Alloy.Globals.is_rtl();


fetch_courses();
update_bar();
if( rtl ) { handle_rtl_changes(); };
$.courses_table.addEventListener('click', fetch_course_statistics);
function fetch_courses(){
	var callback = function(courses_hash){
		auto_complete.auto_complete($.courses_search, $.courses_table, courses_hash, $.charts_courses_label, "charts", rtl);
	};
	requester.get("api/courses/",{},callback);	
};

function update_bar(){
	top_bar.children[1].text = L("charts_view_title");
	right_button = top_bar.children[2];
	if(right_button){top_bar.remove(right_button);};
	
};
function fetch_course_statistics(e){
	course_id = e.rowData.course_id;
	course_name = e.row.children[0].text;
	var callback = function(statistics){
        open_course_chart(statistics,course_name,course_id);		
	};
	requester.get("api/courses/"+course_id+"/statistics/",{},callback);	
};
function open_course_chart(statistics, course_name, course_id){ 
	var bootstaped_window = create_win(course_name, course_id);
	var chart_web_view = create_chart(statistics);
	bootstaped_window.add(chart_web_view);
	bootstaped_window.open();
}
function create_win(course_name, course_id){
	win = Ti.UI.createWindow({
		backgroundImage: "/images/dark_menu_bg.jpg",
		fullscreen: true,
		navBarHidden : true,
		modal: true,
		layout : "vertical"
	});
	header_view = create_header_view(course_name, win);
	win.add(header_view);
	return win ;
	
};
function create_chart(statistics){
	xPoints = statistics[0] || [];
	yPoints = statistics[1] || [];
	chart =  Ti.UI.createWebView({
		url: '/chart.html',
		backgroundColor:"trnasparent",
		backgroundImage:"/images/transparentWhite.png",
		top: "20%",
		height: '60%',
		width: "94%"
	});
	chart.addEventListener("load",function(e){
		Ti.App.fireEvent("app:drawChart",{xpts:xPoints,ypts:yPoints,height:Ti.Platform.displayCaps.platformHeight*.5,width:Ti.Platform.displayCaps.platformWidth*.8});
	});
	
	return chart;
};
function create_header_view(course_name, win){
	header_view = Ti.UI.createView({
		backgroundColor:"#ff8900",
		height:"44",
		top:"0"
	});
	bar_label= Ti.UI.createLabel({
		text: course_name,
		color:"white"
	});
	back_button = Ti.UI.createButton({
		backgroundColor: "transparent",
		backgroundImage: "/images/back_icon.png",
		left:"2",
		top:"2",
		height:"40",
		width:"40"
	});
	back_button.addEventListener("click",function(e) {win.close();} );
	header_view.add(back_button);
	header_view.add(bar_label);
	
	return header_view ;
};

function handle_rtl_changes(){
	$.courses_search.backgroundImage = "/images/search_field_bg_rtl.png";
	$.courses_search.textAlign="right";
};
