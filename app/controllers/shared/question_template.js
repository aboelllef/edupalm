var args = arguments[0]||{};
var manuscript = args["manuscript"];
var choices = manuscript["possible_answers"];
var order = args.order;
var paragraph = args.paragraph;
var show_correct = args.show_correct;
var rtl = Alloy.Globals.is_rtl();
form_questions();
var database = require('database');

function toggle_background (e,that) {
	if (rtl) {
		for (var i=0; i < choices.length; i++) {
			that.data[0].rows[i].rightImage = '/images/quizzes_icons/off.png';
		};
		e.row.rightImage = '/images/quizzes_icons/on.png';		
	} else{
		for (var i=0; i < choices.length; i++) {
			that.data[0].rows[i].leftImage = '/images/quizzes_icons/off.png';
		};
		e.row.leftImage = '/images/quizzes_icons/on.png';
	};	
};

function form_questions(){
	var choices_data = [];
	for (var i=0; i < choices.length; i++) {
		var row = Ti.UI.createTableViewRow({ choice_id:choices[i].id,manuscript_id:manuscript.id});
		$.addClass(row,"choices_row"); 
	  	var row_title = Ti.UI.createLabel({
	  		text : choices[i].description
	  	});
	  	$.addClass(row_title,"row_label");
	  	if(rtl){
	  		row.rightImage = "/images/quizzes_icons/off.png";
	  		row.leftImage="none";
	  		row_title.textAlign="right";
	  	};
	  	if(show_correct && choices[i].correct_answer){
	  		handle_correct_answer_view(row,row_title);
	  	};
	  	row.add(row_title);
	  	choices_data.push(row);
	  	
	};
	$.choices_table.setData(choices_data);
	if (!show_correct) {
		$.choices_table.addEventListener('click',choices_table_clicked);
	};
	$.question_body.text = manuscript.description;
	$.question_view.question_order = order;
	if (paragraph) {
		$.paragraph.text = paragraph;
	}else{
		$.paragraph_container.height= 0;
		$.paragraph_container.top=0;
	}; 
	if (rtl){
		$.question_body.textAlign="right";
		$.paragraph.textAlign="right";
	};
};

function choices_table_clicked(e) {
	updated = update_student_answers(e.rowData.choice_id, e.rowData.manuscript_id);
	if (updated) { 
		toggle_background(e, this);
		update_unanswered_counter(); 
	} ;
};
function handle_correct_answer_view(row,row_title){
	row_title.color = "#00bb00";
	if (rtl) {
		row.rightImage = '/images/quizzes_icons/on.png';
	} else{
		row.leftImage = '/images/quizzes_icons/on.png';
	};
};


function update_student_answers(choice_id,manuscript_id){
	var answers = Ti.App.Properties.getObject("student_answers");
	answers[manuscript_id]["answer"] = choice_id;
	Ti.App.Properties.setObject('student_answers',answers);
	Ti.API.info(JSON.stringify(Ti.App.Properties.getObject("student_answers")));
	return answers[manuscript_id]['answer'] == choice_id;
	
};
function update_unanswered_counter(){
	var question_ids=[] ;
	var answers = Ti.App.Properties.getObject("student_answers");
	for( var i in answers){
		if (!answers[i].answer) {
			question_ids.push(answers[i].question);
		};
	};
	question_ids = question_ids.filter(uniq)
	Ti.App.fireEvent("answer_clicked",{count: question_ids.length})	
};
function uniq(value, index, self){
	return self.indexOf(value) === index
};

