var args=arguments[0]|| {};
var top_bar=args["top_bar"];
top_bar.children[1].text=L("login");
var left_button = args.left_button;
left_button.visible = false;
right_button = top_bar.children[2];
if(right_button){top_bar.remove(right_button);};
var requester = require("requests_handler");
var username ; 
var password ;
var current_response ;
var client_secret ; 
var client_id ; 
var grant_type ; 
var access_token ;
var pk; 


var rtl = Alloy.Globals.is_rtl();
if( rtl ) { handle_rtl_changes(); };

function first_request_handler(json){
	client_secret = json["client_secret"];
	client_id = json["client_id"];
	grant_type = json["grant_type"];
	pk = json["client_pk"];
};
function get_client_data(){
	var params = { username:username, password: password };
	var callback = function(response){
		Ti.API.info(JSON.stringify(response));
        first_request_handler(response);
        get_access_token();
	};
	var error_callback = function(e){
		alert(e.source.status);
		var message = L("wrong_username");		
		if (e.source.status == 401) {
			alert(message);
		} else{
			Alloy.Globals.handle_network_failure(e,message);
		};
	};
	Ti.API.info(JSON.stringify(params));
	requester.post("oauth/login/",params,callback,error_callback);
};
function get_access_token(){
	var params = { username:username, password: password,
		client_secret:client_secret, client_id:client_id, grant_type:grant_type
	 };
	var callback = function(response){
	    Ti.API.info(response);
	    var access_token = response["access_token"];
	    Alloy.Globals.access_token = access_token;
	    Ti.App.Properties.setString("access_token",access_token);
	    Ti.App.Properties.setString("student_pk",pk);
		Alloy.createController("index").fetch_student_info();
		
	};
	requester.post("oauth/token/",params,callback);
}
function signin() {
	username = $.studentName.value;
	password = $.studentPassword.value;
	if (username && password) {
		get_client_data();
	} else{
		alert(L("mandatory_label"));
	};
};
function handle_rtl_changes(){
	$.studentName.textAlign = "right";
	$.studentPassword.textAlign = "right";
	$.mandatory.textAlign =  "right";
};