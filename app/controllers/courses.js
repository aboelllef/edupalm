var auto_complete = require('autocomplete');
var args = arguments[0]||{};

var top_bar= args["top_bar"];
var container = args["views_container"];
var left_button = args.left_button;
var requester = require("requests_handler");



// handle web requests 
var rtl = Alloy.Globals.is_rtl();
fetch_courses();
function fetch_courses(){
	var callback = function(courses_hash){
		auto_complete.auto_complete($.courses_search, $.courses_table, courses_hash, $.coursesLabel,"courses", rtl);
	};
	requester.get("api/courses/",{},callback);	
};


function open_course_tests(e) {
	var row_label = e.row.children[0].text;
	var course_id = e.rowData["course_id"];
	var callback = function(tests){
		if (tests.length) {
			Ti.App.fireEvent("app:open_tests",{"bar_label": row_label, "tests":tests});	
		} else{
			alert("this course does not cantain any tests yet !!");
		};
	};
	requester.get("api/tests?course="+course_id,{},callback);
};
 
function update_bar(){
	top_bar.children[1].text = L("my_tests");
	left_button.visible = true;
	right_button = top_bar.children[2];
	if(right_button){top_bar.remove(right_button);};	
};
function handle_rtl_changes(){
	$.courses_search.backgroundImage = "/images/search_field_bg_rtl.png";
	$.courses_search.textAlign="right";
};

update_bar();
if( rtl ) { handle_rtl_changes(); };
$.courses_table.addEventListener('click',open_course_tests);



function fetch_tests(course_id){
	url	=  Alloy.Globals.domain_name + "api/tests/?course="+course_id	;
	xhr3 = Ti.Network.createHTTPClient({
		onload:function(e) {
	        tests_hash = JSON.parse(this.responseText);
			form_tests_table(tests_hash);
   		},
	    onerror: function(e) {
	    	Alloy.Globals.handle_network_failure(e);
	    },
	    timeout: 10000
	});
	xhr3.open("GET",url);
	xhr3.setRequestHeader('Authorization','Bearer '+Alloy.Globals.access_token);
		
	xhr3.send();
}