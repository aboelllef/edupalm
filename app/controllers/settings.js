var args = arguments[0] || {};
var top_bar = args["top_bar"];
var container = args["views_container"];
var left_button = args.left_button;

var rtl = Alloy.Globals.is_rtl();
var locales = ["english", "arabic"];

function open_locales_rows(e){
	if (e.index == 0){	
		toggle_expand(e);
	} else {
		change_locale(e);
		toggle_expand(e);
	};
};

function change_locale(e) {
	var row_locale = e.rowData["locale"];
	Ti.App.Properties.setString("custom_locale",row_locale);
	var locale = require('com.shareourideas.locale');
	locale.setLocale(row_locale);
	$.current_locale_label.text = L(locales[e.index]);  
	Ti.App.fireEvent("app:open_settings");
};
function toggle_expand(e) {
	if ($.locales_table.data[0].rows.length > 1) {
		for (var i = 1; i <= $.locales_table.data[0].rows.length; i++) {
			$.locales_table.deleteRow(1);
			$.locales_table.height = parseInt($.locales_table.height) - 42;
		};
	} else {
		for (var i = 0; i < locales.length; i++) {
			var row = Ti.UI.createTableViewRow({
				locale : locales[i].slice(0,2).toLowerCase()
			});
			var row_label = Ti.UI.createLabel({				
				text : L(locales[i]),
				color: 'white',
				width: "80%",
				textAlign:"left"
			});
			if(rtl){row_label.textAlign = "right";}
			$.addClass(row, "row_locales");
			row.add(row_label);
			$.locales_table.insertRowAfter(0, row);
			$.locales_table.height = parseInt($.locales_table.height) + 42;
		};
	};
};
function open_row_related_view(e) {
	var view_name = e.rowData["custom_view"];
	if (view_name == 'settings/reset_password') {
		var win =  Alloy.createController(view_name).getView();
		win.open();
	}else{
		open_custom_win(e, view_name);
	}
};
function open_custom_win(e, view_name) {
	var view = Alloy.createController(view_name).getView();
	var settings_win = Ti.UI.createWindow({
		layout : "vertical"
	});
	var header = Ti.UI.createView({
		layout : "horizontal",
		top: "0dp",
		left: "0dp",
		width : Ti.Platform.displayCaps.platformWidth,
		height : "44",
		backgroundColor: '#dd8016'
	});
	var close_button = Ti.UI.createButton();
	var header_title = Ti.UI.createLabel({
		text : e.row.children[0].text
	});
	
	$.addClass(settings_win, "settings_win");
	$.addClass(close_button, "close_button");
	$.addClass(header_title, "header_title");
	header.add(close_button);
	header.add(header_title);
	
	close_button.addEventListener("click", function(e) {
		settings_win.close();
	});
	settings_win.add(header);
	settings_win.add(view);
	settings_win.open();
};
function handle_rtl_changes(){
	$.locale_row_label.textAlign = "right";
	$.current_locale_label.textAlign = "left";
	$.help_row_label.textAlign = "right";
	$.about_row_label.textAlign = "right";
	$.reset_password_row_label.textAlign = "right";
	$.help_row.backgroundImage = "/images/table_rows/table_row_bg_rtl.png";
	$.about_row.backgroundImage = "/images/table_rows/table_row_bg_rtl.png";
	$.reset_password_row.backgroundImage = "/images/table_rows/table_row_bg_rtl.png";
	$.current_locale_label.text = L(Ti.App.Properties.getString('custom_locale'));
};
function update_bar() {
	top_bar.children[1].text = L("settings_view_title");
	right_button = top_bar.children[2];
	if (right_button) {top_bar.remove(right_button);};
	$.current_locale_label.text = L(Ti.App.Properties.getString('custom_locale'));

};

if( rtl ) { handle_rtl_changes(); };
update_bar();
$.locales_table.addEventListener("click", open_locales_rows);
$.settings_table.addEventListener("click", open_row_related_view);