var tasks = require('commenTasks');
var args = arguments[0]||{};
var top_bar= args["top_bar"];
var container = args["views_container"];
var left_button = args.left_button;
top_bar.children[1].text = args["bar_label"];
var tests = args["tests"];

var rtl = Alloy.Globals.is_rtl();

left_button.visible = false;
var back_button = Ti.UI.createButton({
	backgroundImage:"/images/back_icon.png",
	left:"2",
	top:"2",
	width:"40",
	height:"40"
});
back_button.addEventListener('click',function(e){
	back_button.visible = false;
	left_button.visible = true;
	Ti.App.fireEvent("app:open_home");
});
top_bar.add(back_button);

form_tests_table(tests);

function form_tests_table(tests){
	var table_data=[];
	for (var i = 0 ; i < tests.length ; i++){
		
		var title = Ti.UI.createLabel({
			text:tests[i].title,
			color:'white',
			width:'75%',
			height:100,
			textAlign: "left"
		});
		var row = Ti.UI.createTableViewRow({
			leftImage : get_background_image(tests[i].can_answer_quiz,tests[i].answered_before),
			height:40,
			test_id:tests[i].id,
			can_answer : tests[i].can_answer_quiz
		});
		if(rtl){
			title.textAlign="right";
			row.rightImage = get_background_image(tests[i].can_answer_quiz,tests[i].answered_before);
			row.leftImage = "";
		};
		
		row.add(title);
		table_data.push(row);
	}
	$.tests_table.setData(table_data);
	
	$.tests_table.addEventListener("click",open_quiz_info);

}
function open_quiz_info(e){
	if (e.rowData.can_answer) {
		var requester = require('requests_handler');
		var row_label = e.rowData.test_title;
		var test_id = e.rowData.test_id;
		var callback = function(test){
			back_button.visible = false;
			var row_label = e.row.title;
			Ti.App.fireEvent("app:open_test_info",{"bar_label": row_label,"course_title":args["bar_label"],'test':test,"tests":tests});
		};
		requester.get("api/tests/"+test_id+"/",{},callback);
	} else{
		show_modal_window();
	};
};

function get_background_image(can_answer,answered_before){
	var image ;
	if (! can_answer ){
		image =  "/images/table_rows/red_icon.png";
	}else{
		if(!answered_before){
			image = "/images/table_rows/green_icon.png";
		}else{
			image = "/images/table_rows/yellow_icon.png";
		}
	}
	return image;
}

function show_modal_window(){
	var modal_win = Ti.UI.createWindow({
		modal : true,
		backgroundColor : "#000",
		opacity : "0.7",
		navBarHidden : true
	});

	var container_view = Ti.UI.createView({
		height : "170",
		width : "80%",
		backgroundImage : '/images/box_text.png',
	});
	
	var message_lable = Ti.UI.createLabel({
		text : L("test_not_premitted"),
		color : "white",
		top : "5%",
		width : "auto",
		height : 'auto',
		textAlign : 'center',
		font : {
			fontSize : "18sp"
		}
	});
	
	var exit_button = Ti.UI.createButton({
		title : L("ok_button"),
		bottom : "7%",
		width : "100",
		height : "40",
		color : '#ffffff',
		backgroundColor : "#FF9100"
	});
	
	container_view.add(message_lable);
	container_view.add(exit_button);
	modal_win.add(container_view);
	modal_win.open();
	
	exit_button.addEventListener('click', function(e) {
		modal_win.close();
	});
};
