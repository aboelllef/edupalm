var args = arguments[0] || {};
var top_bar = args["top_bar"];
var container = args["views_container"];
var left_button = args.left_button;
var test = args["test"];
var tests = args.tests;
var course_id = args["course_id"];
var course_title = args["course_title"];
var duration;

left_button.visible = false;

var back_button = Ti.UI.createButton({
	backgroundImage:"/images/back_icon.png",
	left:"2",
	top:"2",
	width:"40",
	height:"40"
});
back_button.addEventListener('click',function(e){
	top_bar.remove(this);
	top_bar.remove(start_quiz_button) ;
	Ti.App.fireEvent("app:open_tests",{"bar_label": course_title, "tests":tests});
});

var start_quiz_button = Ti.UI.createButton({visible : false});
$.addClass(start_quiz_button,"right_button");
start_quiz_button.addEventListener("click", fetch_test_questions);

var rtl = Alloy.Globals.is_rtl();
show_test_details(test);

function show_test_details(test){
	$.quiz_info_text.text = L( "description" ) + test.description;
	var hours = parseInt(test.duration/60);
	var minutes = parseInt(test.duration -(hours*60));
	$.quiz_info_duration.text = L("duration") +"  "+ hours+ " : "+ minutes ;
	start_quiz_button.visible = true;
};
top_bar.children[1].text = L("test_details");
if(rtl){
	$.quiz_info_text.textAlign = "right";
	$.quiz_info_duration.textAlign = "right";
};		

function fetch_test_questions(e){
	requester = require("requests_handler");
	var callback = function(questions){
		if (questions.length) {			
			top_bar.remove(back_button);
			top_bar.remove(start_quiz_button);
			Ti.App.fireEvent("app:startQuiz",{"test_id":test.id,'duration':test.duration, "questions": questions});
		} else{
			alert("this test does not contain any question yet");
		};
	};
	requester.get("api/tests/"+test.id+"/questions/",{},callback)
};


top_bar.add(start_quiz_button);
top_bar.add(back_button);

