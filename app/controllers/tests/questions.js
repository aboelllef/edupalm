var args = arguments[0] || {};
var top_bar = args["top_bar"];
var container = args["views_container"];
var left_button = args.left_button;
var test_id = args["test_id"];
var duration = args["duration"];
var questions = args.questions

var pageControllers;
var rtl = Alloy.Globals.is_rtl();
var scrollableViews = [];
var correct_answers ;
former = require('leftMenu');
former.init(container, top_bar, left_button);

var database = require('database');

var timer = require('timer'); 
timer.setTimer(duration);
update_unanswered_counter({count:questions.length})
drow_questions_view();

Ti.App.addEventListener("app:submit_test",function(data){
	//var answers = database.prepare_answers_to_submit();
	var answers = prepare_answers_to_submit();
	submit_answers(answers);
	
});
Ti.App.addEventListener("app:view_correct_answers",function(){
	former.openView("tests/correct_answers",{test_id:test_id});
	
});
function drow_questions_view(){	
	questions = filter_none_questions(questions);
	formatQuestions(questions);
	init_timer();
	seed_db(questions);
};

function init_timer(){
	timer.start();
	updateTime();
	setInterval(updateTime, 1000);
}

function updateTime() {
	$.timer.text = timer.remainningTime();
};
function seed_db(questions){
	//database.clear();	
	Ti.App.Properties.setObject('student_answers',{});
	var answers  = {};
	for (var i=0; i < questions.length; i++) {
	  for (var j=0; j < questions[i].manuscripts.length; j++) {
		// try{
			// database.add(questions[i].manuscripts[j].id, questions[i].id  , null , null);
		// } catch (e) {
		// Ti.API.info(question[i]);
		// }
		answers[questions[i].manuscripts[j].id] = {"question":questions[i].id,"correct":null,"answer":null};
	 };
	};
	Ti.App.Properties.setObject('student_answers',answers);
	
};

function update_bar(questions_length, top_bar, left_menu){
	top_bar.children[1].text = L("question") + "1" + L("of") + questions_length;
	left_button.visible = false;
	top_bar.remove(top_bar.children[2]);
};

function formatQuestions(questions){
	for (var i=0; i < questions.length; i++) {
		for (var j=0; j < questions[i]["manuscripts"].length; j++) {
			var question_view = Alloy.createController("shared/question_template",
				{ manuscript: questions[i]["manuscripts"][j],order: i+1,paragraph: questions[i].description }
			).getView();
			scrollableViews.push(question_view);
		};
	};
	$.questionsScrollableView.setViews(scrollableViews);
	var win = Alloy.Globals.win;
	var pageControllers=require('customScrollableView').pagingControl($.questionsScrollableView, top_bar, questions.length,true);
	win.add(pageControllers[0]);
	win.add(pageControllers[1]);
	update_bar(questions.length,top_bar,left_button);	
};


function remove_questions_related_and_view_score(data){

};
function submit_answers(answers){
	var requester =require('requests_handler');	
	var callback = function(response){
		var degree = response.degree;
		var total_degree = response.total_degree; 
		var view_answers = response.can_view_answers;
		timer.stop();
		if (pageControllers) {
			pageControllers[1].hide();
			pageControllers[0].hide();
		};
		former.openView('tests/test_score', {degree:degree, total_degree:total_degree,view_answers:view_answers});
	
	};
	requester.post("api/tests/"+test_id+"/questions/",answers,callback); 	
};

function filter_none_questions(questions){
	filtered_questions = [];
	for (var i=0; i < questions.length; i++) {
	  if (questions[i].manuscripts) {
	  	filtered_questions.push(questions[i]);
	  }else{
	  	Ti.API.info(JSON.stringify(questions[i]));
	  }
	};
	return filtered_questions;	
};

function prepare_answers_to_submit(){
	var student_answers = Ti.App.Properties.getObject('student_answers');
	var answers ={};
	for (var i in student_answers) {
		var string_key = i.toString();
		var key = "question-" + student_answers[string_key]['question'] +
			"-manuscript-" + string_key +
			"-answer";
		answers[key] = student_answers[string_key]['answer']||'';
	}; 
	return answers ;
};
Ti.App.addEventListener('answer_clicked',update_unanswered_counter);

function update_unanswered_counter(data){
	var count = data.count;
	$.unanswered_label.text = count +" / "+ questions.length + L('unanswered_yet');
};
