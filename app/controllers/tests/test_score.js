var tasks = require('commenTasks');

var args = arguments[0]||{};
var top_bar= args["top_bar"];
var container = args["views_container"];
var degree = args.degree;
var total_degree = args.total_degree;
var can_view_answers = args.view_answers;
var left_button = args.left_button;
var back_button = Ti.UI.createButton({
	width: "40",
	height:"40",
	left:'2',
	top: '2',
	backgroundImage:"/images/back_icon.png",
	backgroundColor: "transparent"
});
back_button.addEventListener("click",function(e){
	top_bar.remove(this);
	Ti.App.fireEvent("app:open_home");
});
top_bar.add(back_button);
$.test_score.text = degree + " / " + total_degree;
if(can_view_answers){
	$.view_answers.addEventListener("click",function(e) {
		Ti.App.fireEvent("app:view_correct_answers");
		back_button.hide();
		
	});
}else{
	$.view_answers.visible = false;
}

update_bar();


function update_bar(){
	top_bar.children[1].text = L("score");
	right_button = top_bar.children[2];
	if(right_button){top_bar.remove(right_button);};	
};
