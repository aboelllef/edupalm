var args = arguments[0] || {};
var top_bar = args["top_bar"];
var container = args["views_container"];
var left_button = args.left_button;
var test_id =args.test_id;

var pageControllers;
var rtl = Alloy.Globals.is_rtl();
var scrollableViews = [];
var correct_answers ;
former = require('leftMenu');
former.init(container, top_bar, left_button);

fetch_questions_with_correct_answers(test_id);


function fetch_questions_with_correct_answers(test_id){
	var requester = require("requests_handler");
	var callback = function(questions){
			formatQuestions(questions);		
	}
	requester.get("api/tests/"+test_id+"/correct_answers/",{},callback);
};

function formatQuestions(questions){
	questions = filter_none_questions(questions);
	for (var i=0; i < questions.length; i++) {
		for (var j=0; j < questions[i]["manuscripts"].length; j++) {
			var question_view = Alloy.createController("shared/question_template",
				{ manuscript: questions[i]["manuscripts"][j],order: i+1,
				paragraph: questions[i].description,show_correct:true}
			).getView();
			scrollableViews.push(question_view);
		};
	};
	$.correct_answers_scrollable_view.setViews(scrollableViews);
	var win = Alloy.Globals.win;
	pageControllers=require('customScrollableView').pagingControl($.correct_answers_scrollable_view, top_bar,
																	 questions.length,false);
	win.add(pageControllers[0]);
	win.add(pageControllers[1]);
	update_bar(questions.length,top_bar,left_button);	
};
function update_bar(questions_length, top_bar, left_menu){
	top_bar.children[1].text = L("question") + "1" + L("of") + questions_length;
	left_button.hide();
	for (var i=2; i < top_bar.children.length; i++) {
	  top_bar.remove(top_bar.children[i]);
	};
};


function filter_none_questions(questions){
	filtered_questions = [];
	for (var i=0; i < questions.length; i++) {
	  if (questions[i].manuscripts) {
	  	filtered_questions.push(questions[i]);
	  }else{
	  	Ti.API.info(JSON.stringify(questions));
	  }
	};
	return filtered_questions;
	
};

