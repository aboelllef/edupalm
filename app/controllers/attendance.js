var rtl = Alloy.Globals.is_rtl();
var args = arguments[0]||{};
var top_bar= args["top_bar"];
var container = args["views_container"];
var left_button = args.left_button;

is_attended_today();
function is_attended_today(){
	var requester = require("requests_handler");
	var callback = function(response){
		if (response.attendance) {
			show_message("attended_before");
			top_bar.remove(top_bar.children[2]);
		};
	};
	requester.get("api/attendanceconfirmation",{},callback);
};
function create_message(text){
	return Ti.UI.createLabel({
		text: L(text),
		backgroundImage:"/images/transparentWhite.png",
		color: "white",
		top: "50%",
		textAlign: "center",
		width:"80%"
	});
};

function show_message(text){
	var children_length = $.attendanceView.children.length;
	for (var i=0; i < children_length; i++) {
	  $.attendanceView.remove($.attendanceView.children[children_length-i-1]);
	};
	var message = create_message(text);
	$.attendanceView.add(message);
};

function create_submit_button(){
	var button = Ti.UI.createButton();
	$.addClass(button,"right_button");
	button.addEventListener("click",submit_attendance_data);
	return button;
}; 

function getDate() {
	var currentTime = new Date();
	var month = currentTime.getMonth() + 1;
	var day = currentTime.getDate();
	var year = currentTime.getFullYear(); 
	return day+"/ "+month+"/ "+year ;
};
function submit_attendance_data(){
	var requester = require("requests_handler");
	var code =  $.attendanceCodeField.value; 
	if ( code.replace(/\s/g, "") ) {
		var callback = function(){
			show_message("success_text");
			top_bar.remove(top_bar.children[2]);
		};
		var error_callback = function(){
			var message = L("wrong_attendamce_code");
			Alloy.Globals.handle_network_failure(e,message);
		};
		requester.post("api/attendanceconfirmation/",{code:code},callback,error_callback);
	} else{
		alert(L("alert_attendance_empty_field"));
	};
};
function update_bar(){
	top_bar.children[1].text = L("atttendance_view_title");
	right_button = top_bar.children[2];
	if(right_button){top_bar.remove(right_button); };
	button = create_submit_button();
	top_bar.add(button);
};
function handle_rtl_changes(){
	$.attendanceCodeField.textAlign = "right";
	$.attendanceDescriptionLabel.textAlign = "right";
	$.mandatory_label.textAlign='right';
}

$.date_label.text = L("date")+getDate();
update_bar();
if( rtl ) { handle_rtl_changes(); };

